!#/bin/bash

echo "Patching HWC"
patch < .repo/local_manifests/patches/frameworks_native-hwc.patch frameworks/native/services/surfaceflinger/SurfaceFlinger.cpp

echo "Patching Carbon vendor"
patch < .repo/local_manifests/patches/vendor_carbon-kmod.patch vendor/carbon/build/tasks/kernel.mk

echo "Applying patch for RSMouse"
patch < .repo/local_manifests/patches/frameworks_base-rsmouse.patch frameworks/base/core/java/android/view/ViewRootImpl.java

echo "Patching Nvidia vendor"
patch < .repo/local_manifests/patches/0001-HACK-use-platform-sig-for-shieldtech.patch vendor/nvidia/shield/shieldtech/Android.mk
cp .repo/local_manifests/patches/NvShieldTech-hack.apk vendor/nvidia/shield/shieldtech/app/NvShieldTech.apk
