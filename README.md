# Switch manifest for CarbonROM 7.0

### Issues
* Random graphics glitches without hwc patch.

### Patching
* Repopick these topics from the CarbonROM gerrit:
  * nvidia-enhancements-p
  * joycon-p
  * icosa-bt
* Run apply-patches.sh from the top of the build directory
